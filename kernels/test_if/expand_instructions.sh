#!/bin/bash

# Helper script for programmatically appending instructions to test_if.cu
# in order to have enough instructions that will saturate the instruction cache.

tmp_filename=dummy_instructions.txt
touch $tmp_filename
for i in {1..128}; do
    #echo "arr_d[index]+=$i; arr_d[index]-=$i;" >>$tmp_filename
    echo "arr_d[index]+=1; arr_d[index]-=1;" >>$tmp_filename
done
sed "/DO NOT REMOVE THIS COMMENT/r ${tmp_filename}" test_if.cu.template >test_if.cu
rm $tmp_filename
