
Fatbin elf code:
================
arch = sm_70
code version = [1,7]
producer = <unknown>
host = linux
compile_size = 64bit

	code for sm_70

Fatbin ptx code:
================
arch = sm_70
code version = [7,0]
producer = <unknown>
host = linux
compile_size = 64bit
compressed

Fatbin elf code:
================
arch = sm_70
code version = [1,7]
producer = <unknown>
host = linux
compile_size = 64bit

	code for sm_70
		Function : _Z5dummyv
	.headerflags    @"EF_CUDA_SM70 EF_CUDA_PTX_SM(EF_CUDA_SM70)"
        /*0000*/                   IMAD.MOV.U32 R1, RZ, RZ, c[0x0][0x28] ;  /* 0x00000a00ff017624 */
                                                                            /* 0x000fd200078e00ff */
        /*0010*/              @!PT SHFL.IDX PT, RZ, RZ, RZ, RZ ;            /* 0x000000fffffff389 */
                                                                            /* 0x000fe200000e00ff */
        /*0020*/                   S2R R0, SR_CTAID.X ;                     /* 0x0000000000007919 */
                                                                            /* 0x000e220000002500 */
        /*0030*/                   IADD3 R1, R1, -0x8, RZ ;                 /* 0xfffffff801017810 */
                                                                            /* 0x000fe40007ffe0ff */
        /*0040*/                   MOV R4, 0x0 ;                            /* 0x0000000000047802 */
                                                                            /* 0x000fe20000000f00 */
        /*0050*/                   S2R R3, SR_TID.X ;                       /* 0x0000000000037919 */
                                                                            /* 0x000e220000002100 */
        /*0060*/                   IADD3 R6, P0, R1, c[0x0][0x20], RZ ;     /* 0x0000080001067a10 */
                                                                            /* 0x000fe40007f1e0ff */
        /*0070*/                   MOV R5, 0x0 ;                            /* 0x0000000000057802 */
                                                                            /* 0x000fc60000000f00 */
        /*0080*/                   IMAD.X R7, RZ, RZ, c[0x0][0x24], P0 ;    /* 0x00000900ff077624 */
                                                                            /* 0x000fe400000e06ff */
        /*0090*/                   IMAD R0, R0, c[0x0][0x0], R3 ;           /* 0x0000000000007a24 */
                                                                            /* 0x001fd000078e0203 */
        /*00a0*/                   STL [R1], R0 ;                           /* 0x0000000001007387 */
                                                                            /* 0x0001e40000100800 */
        /*00b0*/                   MOV R20, 0x0 ;                           /* 0x0000000000147802 */
                                                                            /* 0x000fe40000000f00 */
        /*00c0*/                   MOV R21, 0x0 ;                           /* 0x0000000000157802 */
                                                                            /* 0x000fd20000000f00 */
        /*00d0*/                   CALL.ABS.NOINC 0x0 ;                     /* 0x0000000000007943 */
                                                                            /* 0x001fea0003c00000 */
        /*00e0*/                   EXIT ;                                   /* 0x000000000000794d */
                                                                            /* 0x000fea0003800000 */
        /*00f0*/                   BRA 0xf0;                                /* 0xfffffff000007947 */
                                                                            /* 0x000fc0000383ffff */
		....................


