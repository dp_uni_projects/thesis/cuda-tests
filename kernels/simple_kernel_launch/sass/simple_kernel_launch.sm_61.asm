
Fatbin elf code:
================
arch = sm_61
code version = [1,7]
producer = <unknown>
host = linux
compile_size = 64bit

	code for sm_61

Fatbin ptx code:
================
arch = sm_61
code version = [7,0]
producer = <unknown>
host = linux
compile_size = 64bit
compressed

Fatbin elf code:
================
arch = sm_61
code version = [1,7]
producer = <unknown>
host = linux
compile_size = 64bit

	code for sm_61
		Function : _Z5dummyv
	.headerflags    @"EF_CUDA_SM61 EF_CUDA_PTX_SM(EF_CUDA_SM61)"
                                                                           /* 0x001c4400fe0007f6 */
        /*0008*/                   MOV R1, c[0x0][0x20] ;                  /* 0x4c98078000870001 */
        /*0010*/         {         IADD32I R1, R1, -0x8 ;                  /* 0x1c0fffffff870101 */
        /*0018*/                   S2R R0, SR_CTAID.X         }
                                                                           /* 0xf0c8000002570000 */
                                                                           /* 0x001fd000e62007f0 */
        /*0028*/         {         MOV32I R4, 0x0 ;                        /* 0x010000000007f004 */
        /*0030*/                   S2R R2, SR_TID.X         }
                                                                           /* 0xf0c8000002170002 */
        /*0038*/                   MOV32I R5, 0x0 ;                        /* 0x010000000007f005 */
                                                                           /* 0x003f8400fe2007f6 */
        /*0048*/                   IADD R6.CC, R1, c[0x0][0x4] ;           /* 0x4c10800000170106 */
        /*0050*/                   IADD.X R7, RZ, c[0x0][0x104] ;          /* 0x4c1008000417ff07 */
        /*0058*/                   XMAD.MRG R3, R0, c[0x0] [0x8].H1, RZ ;  /* 0x4f107f8000270003 */
                                                                           /* 0x0003d000fe4217f6 */
        /*0068*/                   XMAD R2, R0.reuse, c[0x0] [0x8], R2 ;   /* 0x4e00010000270002 */
        /*0070*/                   XMAD.PSL.CBCC R0, R0.H1, R3.H1, R2 ;    /* 0x5b30011800370000 */
        /*0078*/                   STL [R1], R0 ;                          /* 0xef54000000070100 */
                                                                           /* 0x001ffc00fd600ffd */
        /*0088*/                   JCAL 0x0 ;                              /* 0xe220000000000040 */
        /*0090*/                   NOP ;                                   /* 0x50b0000000070f00 */
        /*0098*/                   EXIT ;                                  /* 0xe30000000007000f */
                                                                           /* 0x001f8000fc0007ff */
        /*00a8*/                   BRA 0xa0 ;                              /* 0xe2400fffff07000f */
        /*00b0*/                   NOP;                                    /* 0x50b0000000070f00 */
        /*00b8*/                   NOP;                                    /* 0x50b0000000070f00 */
		....................


