#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

__global__ void dummy() {
    // 1D grid, 1D blocks
    uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;
    printf("%lu\tOMG DRAMA\n", tid);
}

int main() {
        int blockSize = 512;
        int gridSize = 1;

        dummy<<<gridSize, blockSize>>>();

        cudaError_t err = cudaGetLastError();
        if (err != cudaSuccess) {
            printf("%s\n", cudaGetErrorString(err));
            return 1;
        }
        printf("Test PASSED\n");
        return 0;
}